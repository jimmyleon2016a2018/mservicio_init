const Controllers = require("../controllers");
const { InternalError } = require("../settings");

async function Create({ name, age, color }) {
    try {
        let { statusCode, data, message } = await Controllers.Create({ name, age, color });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service Create", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function Delete({ id }) {
    try {
        //buscar el usuario para ver si existe 
        const findOne = await Controllers.FindOne({ where: { id } });
        if (findOne.statusCode !== 200) {
            //lo mismo que un switch si es 400 retorne algo si es 500 retorne algo
            /*let Response ={
                400:{statusCode:400, message:"the user does not exist"},
                500:{statusCode:500, message: InternalError}
            }
            return Response[findOne.statusCode]*/
            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "the user does not exist" }
                case 500: return { statusCode: 500, message: InternalError }
                default: return { statusCode: findOne.statusCode, message: findOne.message }
            }
            
        }

        let del = await Controllers.Delete({ where: { id } });
        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };
        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: " service Delete", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function Update({ name, age, color, id }) {
    try {
        let { statusCode, data, message } = await Controllers.Update({ name, age, color, id });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service Update", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function FindOne({ name }) {
    try {
        let { statusCode, data, message } = await Controllers.FindOne({ where: { name } });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service FindOne", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function View({ }) {
    try {
        let { statusCode, data, message } = await Controllers.View({});
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service View", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function ExistUser({ id }) {
    try {
        let { statusCode, data, message } = await Controllers.ExistUser({ where: { id } });
        if (data !== true) throw ("User no found")
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service ExistUser", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
module.exports = { Create, Delete, Update, FindOne, View, ExistUser };