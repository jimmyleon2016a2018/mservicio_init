const bull = require('bull');
const { redis } = require('../settings');

//nos comunicamos con el mundo exterior
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull("curso:create", opts)
queueDelete = bull("curso:delete", opts)
queueUpdate = bull("curso:update", opts)
queueFindOne = bull("curso:findOne", opts)
queueView = bull("curso:view", opts)
queueExistUser = bull("curso:ExistUser", opts)

module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView,queueExistUser }