const Service = require("../services");
const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne, queueExistUser } = require('./index');

async function Create(job, done) {

    try {
        const { name, age, color } = job.data;
        let { statusCode, data, message } = await Service.Create({ name, age, color });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueCreate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Delete(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Delete({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueDelete", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Update(job, done) {

    try {
        const { name, age, color, id } = job.data;
        let { statusCode, data, message } = await Service.Update({ name, age, color, id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueUpdate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function FindOne(job, done) {

    try {
        const { name } = job.data;
        let { statusCode, data, message } = await Service.FindOne({ name });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueFindOne", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function View(job, done) {

    try {
        const { } = job.data;
        console.log("respons: ", job.id);

        let { statusCode, data, message } = await Service.View({});
        done(null, { statusCode, data: data.map(v => ({ ...v.toJSON(), ...{ worker: job.id } })), message })

    } catch (error) {
        console.log({ step: "adapter process queueView", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function ExistUser(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.ExistUser({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueExistUser", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
/*esta funcion forza el arranque por esta razon hay que exportar las funciones de procesos
de esta manera un archivo externo se encarga de inicializarlo*/
async function run() {
    try {
        queueCreate.process(Create)
        queueDelete.process(Delete)
        queueUpdate.process(Update)
        queueFindOne.process(FindOne)
        queueView.process(View)
        queueExistUser.process(ExistUser)

        console.log('inicializando worker');

    } catch (error) {
        console.log(error);

    }
}
module.exports = { Create, Delete, Update, FindOne, View, ExistUser, run }