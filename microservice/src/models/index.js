const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
//este es nuestro modelo

const Model = sequelize.define("curso", {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING }
});
async function SyncDB() {
    try {
        console.log('inicializando base de datos');
        await Model.sync({ logging: false });     //{ logging: false } desavilita el login de inicio
        console.log('base de datos inicializada');
        return { statusCode: 200, data: "ok" }
    } catch (error) {
        console.log(error);
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { SyncDB, Model };