const { io } = require("socket.io-client");
const socket = io("http://localhost");

async function main() {
    try {
        socket.on("res:microservice:view", ({ statusCode, data, message }) => {
            console.log("res:microservice:view", { statusCode, data, message });
        })
        socket.on("res:microservice:create", ({ statusCode, data, message }) => {
            console.log("res:microservice:create", { statusCode, data, message });
        })
        socket.on("res:microservice:findOne", ({ statusCode, data, message }) => {
            console.log("res:microservice:findOne", { statusCode, data, message });
        })
        socket.on("res:microservice:update", ({ statusCode, data, message }) => {
            console.log("res:microservice:update", { statusCode, data, message });
        })
        socket.on("res:microservice:delete", ({ statusCode, data, message }) => {
            console.log("res:microservice:delete", { statusCode, data, message });
        })
        socket.on("res:microservice:existUser", ({ statusCode, data, message }) => {
            console.log("res:microservice:existUser", { statusCode, data, message });
        })

        setTimeout(() => {
            //socket.emit("req:microservice:create",({name:"lola",age:30,color:"blanco"}));
            //socket.emit("req:microservice:update",({name:"lola",age:30,color:"rosado", id:12}));
            //socket.emit("req:microservice:findOne",({name:"lola"}));
            //socket.emit("req:microservice:delete",({id:10}));
            //socket.emit("req:microservice:existUser",({id:11}));
            socket.emit("req:microservice:view", ({}));
        }, 300);


    } catch (error) {
        console.log(error);

    }
}
main();