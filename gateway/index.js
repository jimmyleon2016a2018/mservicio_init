const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server);

const api = require("../api");

server.listen(80, () => {
    console.log('server initializa');
    io.on('connection', socket => {
        console.log('new connection', socket.id);

        socket.on('req:microservice:view', async ({ }) => {
            try {
                console.log('req:microservice:view');
                const { statusCode, data, message } = await api.View({});
                return io.to(socket.id).emit('res:microservice:view', ({ statusCode, data, message }));

            } catch (error) {
                console.log(error);

            }

        })
        socket.on('req:microservice:create', async ({ name, age, color }) => {

            try {
                console.log('req:microservice:create', { name, age, color });
                const { statusCode, data, message } = await api.Create({ name, age, color });
                return io.to(socket.id).emit('res:microservice:create', ({ statusCode, data, message }));

            } catch (error) {
                console.log(error);

            }
        })
        socket.on('req:microservice:findOne', async ({ name }) => {

            try {
                console.log('req:microservice:findOne', { name });
                const { statusCode, data, message } = await api.FindOne({ name });
                return io.to(socket.id).emit('res:microservice:findOne', ({ statusCode, data, message }))

            } catch (error) {
                console.log(error);

            }

        })
        socket.on('req:microservice:update', async ({ name, age, color, id }) => {

            try {
                console.log('req:microservice:update', { name, age, color, id });
                const { statusCode, data, message } = await api.Update({ name, age, color, id });
                return io.to(socket.id).emit('res:microservice:update', ({ statusCode, data, message }))

            } catch (error) {
                console.log(error);

            }
        })
        socket.on('req:microservice:delete', async ({ id }) => {

            try {
                console.log('req:microservice:delete', { id });
                const { statusCode, data, message } = await api.Delete({ id });
                return io.to(socket.id).emit('res:microservice:delete', ({ statusCode, data, message }))

            } catch (error) {
                console.log(error);

            }
        })
        socket.on('req:microservice:existUser', async ({ id }) => {

            try {
                console.log('req:microservice:existUser', { id });
                const { statusCode, data, message } = await api.ExistUser({ id });
                return io.to(socket.id).emit('res:microservice:existUser', ({ statusCode, data, message }))

            } catch (error) {
                console.log(error);

            }
        })

    })

});