const bull = require('bull');

const redis = { host: "localhost", port: 6379 }
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull("curso:create", opts)
queueDelete = bull("curso:delete", opts)
queueUpdate = bull("curso:update", opts)
queueFindOne = bull("curso:findOne", opts)
queueView = bull("curso:view", opts)
queueExistUser = bull("curso:ExistUser", opts)

async function Create({ name, age, color }) {
    try {
        const job = await queueCreate.add({ name, age, color });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function Delete({ id }) {
    try {
        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.log(error);
    }
};
async function Update({ name, age, color, id }) {
    try {
        const job = await queueUpdate.add({ name, age, color, id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function FindOne({ name }) {
    try {
        const job = await queueFindOne.add({ name });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function View({ }) {
    try {

        const job = await queueView.add({});

        const { statusCode, data, message } = await job.finished();
        console.log(statusCode, data, message);
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function ExistUser({ id }) {
    try {
        const job = await queueExistUser.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.log(error);
    }
};

module.exports = { Create, Delete, Update, FindOne, View, ExistUser }